# sqlite3fs

Virtual Sqlite3 FS

    const vfs = require("sqlite3fs");
    const fs = new vfs({ path: "./filesystem.dbfs", name: "filesystem_table_name" });
    
    fs.open({ path, name }); -> open the filesystem
    fs.close(); -> close the filesystem
    
    fs.read(path); -> file contents / error thrown
    fs.write(path, new_data, force = false); -> write to file, force create if not exist
    fs.create(path, ignore = false); -> create file, ?ignore exists error
    fs.delete(path); -> delete file, error if not file or doesnt exists
    
    fs.mkdir(path); -> make a folder, error if exists
    fs.rmdir(path); -> remove folder, error if doesnt exist
    fs.list(path); -> list contents, error if not exist or not folder
    
    fs.exist(path); -> check if path exists
    fs.type(path); -> check path type error if doesnt exist
    fs.resolve(path); -> resolve path
    fs.stat(path); -> get path info, error if not exist