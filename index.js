const sqldb = require("sqlite3objs");
const _path = require("path");
const parsePath = (path) => {
    path = _path.join(path);
    path = path.split("/");
    let last = path.pop();
    if (last !== "") path.push(last);
    return (path.join("/"));
};

class SqlFS {
    constructor(ops = {}) {
        this.options = Object.assign({}, {
            path: "",
            name: "filesystem",
            autoOpen: true
        }, ops);
        this.db = new sqldb();
        if (this.options.autoOpen) this.open();
    }
    open(ops = {}) {
        this.options.path = ops.path || this.options.path;
        this.options.name = ops.name || this.options.name;;
        if (this.db.open(this.options.path)) {
            // this.db.delete(this.options.name);
            this.db.create(this.options.name, {
                id: "<key>",
                name: "<string>",
                type: "<string>",
                parent: "<int>",
                contents: "<text>"
            });
            if (this.db.select(this.options.name).length === 0) {
                this.db.insert(this.options.name, {
                    name: "",
                    type: "folder",
                    parent: 0
                });
            }
            return (true);
        }
        return (false);
    }
    close() {
        return (this.db.close());
    }
    pathToID(path = "") {
        path = this.resolve(path, true);
        path = path.split("/");
        let id = 0;
        path.forEach((name) => {
            if (id === -1) return;
            let s = this.db.select(this.options.name, {
                where: {
                    name,
                    parent: id
                }
            });
            id = s.length ? s[0].id : 0;
        });
        return (id ? id : false);
    }
    idStat(id = 0) {
        const data = this.db.select(this.options.name, {
            where: {
                id
            }
        });
        return (data.length ? data[0] : false);
    }
    recurseIDs(id) {
        const info = this.idStat(id);
        let ids = [];
        const files = this.db.select(this.options.name, {
            where: {
                parent: info.id
            }
        });
        files.forEach(file => {
            ids.push(file.id);
            if (file.type !== "folder") return;
            const _ids = this.recurseIDs(file.id);
            ids = ids.concat(_ids);
        });
        return (ids);
    }
    read(path = "", create = false) {
        path = this.resolve(path);
        let data = this.exist(path);
        if (!data) this.create(path, create);
        data = this.idStat(this.pathToID(path));
        return (data.contents);
    }
    write(path = "", data = "", force = false) {
        path = this.resolve(path);
        const stats = this.idStat(this.pathToID(path));
        if (!stats) this.create(path, force);
        if (stats.type !== "file") throw new Error("Path isn't file!");
        this.db.update(this.options.name, {
            id: stats.id
        }, {
            content: data
        });
        return (true);
    }
    create(path = "", ignore = false) {
        path = this.resolve(path);
        const parent = this.resolve(`${path}/..`);
        if (!parent) throw new Error("Parent doesn't exist!");
        const file = this.idStat(this.pathToID(path));
        if (file && !ignore) throw new Error("File already exists!");
        return (this.db.insert(this.options.name, {
            name: path.split("/").pop(),
            type: "file",
            parent: parent.id
        }));
    }
    delete(path = "") {
        path = this.resolve(path);
        const data = this.idStat(this.pathToID(path));
        if (!data) throw new Error("Path doesn't exist!");
        if (data.type !== "file") throw new Error("Path isn't file!");
        return (this.delete(this.options.name, {
            id: data.id
        }));
    }
    mkdir(path = "") {
        path = this.resolve(path);
        const parent = this.pathToID(`${path}/..`);
        if (!parent) throw new Error("Parent directory doesn't exist!");
        const id = this.pathToID(path);
        if (id) throw new Error("Folder already exists!");
        const o = this.db.insert(this.options.name, {
            name: path.split("/").pop(),
            parent,
            type: "folder"
        });
        return (o);
    }
    rmdir(path = "") {
        path = this.resolve(path);
        const data = this.idStat(this.pathToID(path));
        if (!data) throw new Error("Folder doesn't exist!");
        const ids = this.recurseIDs(data.id);
        ids.push(data.id);
        ids.forEach(id => this.db.remove(this.otpions.name, {
            id
        }));
        return (!this.idStat(data.id));
    }
    list(path = "") {
        path = this.resolve(path);
        const data = this.idStat(this.pathToID(path));
        if (!data) throw new Error("Folder doesn't exist!");
        let names = 0;
        if (data.type === "folder") {
            names = [];
            data = this.db.select(this.options.name, {
                where: {
                    parent: data.id
                }
            });
            data.forEach(file => names.push({
                name: file.name,
                type: file.type
            }));
        }
        if (!names) throw new Error("Path is file!");
        return (names);
    }
    stat(path = "") {
        path = this.resolve(path);
        const data = this.idStat(this.pathToID(path));
        delete data.id;
        delete data.parent;
        delete data.contents;
        return (data);
    }
    type(path = "") {
        path = this.resolve(path);
        return (this.exist(path) ? this.idStat(this.pathToID(path)).type : false);
    }
    exist(path = "") {
        path = this.resolve(path);
        const id = this.pathToID(path);
        return (!!id);
    }
    resolve(path = "", root = false) {
        return (parsePath(`${root?"/":""}${path}`));
    }
}
module.exports = SqlFS;